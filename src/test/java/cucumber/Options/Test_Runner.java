package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

@CucumberOptions(features = "src/test/java/Features", glue = { "stepDefinitions" },tags="@Put_API or @Patch_API or @Post_API or @Get_API or @Delete_API or @Put_API")
public class Test_Runner {

}
//@Patch_API,@Post_API,@Get_API,@Delete_API,@Put_API
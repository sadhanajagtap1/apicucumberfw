Feature: Trigger the Delete API with required parameters 

@Delete_API 
Scenario: Trigger the delete API request with valid request body parameters 
	Given Retrieve the details of delete API with payload
	When Send the delete request
	Then Validate status code for delete API
	And Validate response body parameters for delete API
	
@Delete_API 
Scenario Outline: Test Delete api with multiple data set 
	Given Retrieve the details of delete API with payload
	When Send the delete request
	Then Validate status code for delete API
	And Validate response body parameters for delete API
	
	Examples: 
	|Id|email|first_name|last_name|avatar|
	|15|"jagtapsadhana@reqres.in"|"Sadhana"|"Jagtap"|"https://reqres.in/img/faces/5-image.jpg"|
	|16|"minakshimane@reqres.in"|"Minakshi"|"Mane"|"https://reqres.in/img/faces/6-image.jpg"|